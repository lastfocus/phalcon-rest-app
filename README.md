# phalcon rest app

# install

1) vagrant up

2) setup db configuration in project/app/config/config.php

3) vagrant ssh 

4) composer update in project folder

5) run migrations: vendor/phalcon/devtools/phalcon.php migration run

4) open in browser 192.168.3.4

# available urls

GET /book/$id - show $id book, for example http://192.168.3.4/book/3

GET /book/all - show all books

GET /book/search/$search - show all books with $search in first name or last name

POST /book/add - add new book ( for example postData: first_name=Sam5&phone_number=12 223 44422455&country_code=US)

PUT /book/$id - update $id book 

DELETE /book/$id - delete $id book