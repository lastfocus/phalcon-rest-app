<?php

namespace App\Services;

use App\Models\Books;

/**
 * Business-logic for books
 *
 * Class BooksService
 */
class BooksService extends AbstractService
{
    /** Unable to create book */
    const ERROR_UNABLE_CREATE_BOOK = 11001;

    /* Book not found */
    const ERROR_BOOK_NOT_FOUND = 11002;

    /* No such book */
    const ERROR_INCORRECT_BOOK = 11003;

    /* Unable to update book */
    const ERROR_UNABLE_UPDATE_BOOK = 11004;

    /* Unable to delete book */
    const ERROR_UNABLE_DELETE_BOOK = 11005;

    /**
     * Creating a new book
     *
     * @param array $bookData
    */
    public function createBook(array $bookData)
    {
        try {
            $book = new Books();
            $nowDate = new \DateTime('now');
            $nowTimeStamp = $nowDate->format('Y-m-d H:i:s');

            $result =  $book->setFirstName($bookData['first_name'])
                            ->setLastName($bookData['last_name'])
                            ->setPhoneNumber($bookData['phone_number'])
                            ->setCountryCode($bookData['country_code'])
                            ->setTimezoneName($bookData['timezone_name'])
                            ->setInsertedOn($nowTimeStamp)
                            ->setUpdatedOn($nowTimeStamp)
                            ->create();


            if (!$result) {
                throw new \RuntimeException('Unable to create book', self::ERROR_UNABLE_CREATE_BOOK);
            }

        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(),$e->getCode(),$e);
        }
    }

    /*
     * Updating an existing book
     *
     * @param array $bookData
     */
    public function updateBook(array $bookData)
    {
        try {
            $book = Books::findFirst(
                [
                    'conditions' => 'id = :id:',
                    'bind' => [
                        'id' => $bookData['id']
                    ]
                ]
            );

            $nowDate = new \DateTime('now');
            $nowTimeStamp = $nowDate->format('Y-m-d H:i:s');

            $bookData['first_name'] = (is_null($bookData['first_name']) ? $book->getFirstName() : $bookData['first_name']);
            $bookData['last_name'] = (is_null($bookData['last_name']) ? $book->getLastName() : $bookData['last_name']);
            $bookData['phone_number'] = (is_null($bookData['phone_number']) ? $book->getPhoneNumber() : $bookData['phone_number']);
            $bookData['country_code'] = (is_null($bookData['country_code']) ? $book->getCountryCode() : $bookData['country_code']);
            $bookData['timezone_name'] = (is_null($bookData['timezone_name']) ? $book->getTimezoneName() : $bookData['timezone_name']);

            $result =  $book->setFirstName($bookData['first_name'])
                            ->setLastName($bookData['last_name'])
                            ->setPhoneNumber($bookData['phone_number'])
                            ->setCountryCode($bookData['country_code'])
                            ->setTimezoneName($bookData['timezone_name'])
                            ->setUpdatedOn($nowTimeStamp)
                            ->update();

            if (!$result) {
                throw  new \RuntimeException('Unable to update book', self::ERROR_UNABLE_UPDATE_BOOK);
            }
        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        };
    }

    /*
     * Delete an existing book
     *
     * @param int $bookId
     */
    public function deleteBook($bookId)
    {
        try {
            $book = Books::findFirst(
                [
                    'conditions' => 'id = :id:',
                    'bind' => [
                        'id' => $bookId
                    ]
                ]
            );

            if (!$book) {
                throw new \RuntimeException('Book not found', self::ERROR_BOOK_NOT_FOUND);
            }

            $result = $book->delete();

            if (!$result) {
                throw new \RuntimeException('Unable to delete book', self::ERROR_UNABLE_DELETE_BOOK);
            }
        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /*
     * returns book list
     *
     * @return array
     */
    public function getBookList()
    {
        try {
            $books = Books::find(
                [
                    'conditions' => '',
                    'bind' => [],
                    'columns' => 'id, first_name, last_name, phone_number, country_code, timezone_name',
                ]
            );

            if (!$books) {
                return [];
            }

            return $books->toArray();
        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /*
     * returns book list
     *
     * @param $search
     *
     * @return array
     */
    public function searchBookList($search)
    {
        try {

            $books = [];
            if (!empty($search)) {
                $books = Books::find(
                    [
                        'conditions' => 'first_name LIKE :search: OR last_name LIKE :search:',
                        'bind' => [
                            'search' => '%'.$search.'%'
                        ],
                        'columns' => 'id, first_name, last_name, phone_number, country_code, timezone_name',
                    ]
                );
            }

            if (!$books) {
                return [];
            }

            return $books->toArray();
        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }




    /*
     * return book item
     *
     * @param int $bookId
     *
     * @return array
     */
    public function getBook($bookId)
    {
        try {
            $book = Books::findFirst(
                [
                    'conditions' => 'id = :id:',
                    'bind' => [
                        'id' => $bookId
                    ],
                    'columns' => 'id, first_name, last_name, phone_number, country_code, timezone_name',
                ]
            );

            if (!$book) {
                throw new \RuntimeException('Book not found', self::ERROR_BOOK_NOT_FOUND);
            }

            return $book->toArray();
        } catch (\PDOException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }
}