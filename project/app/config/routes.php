<?php

$booksCollection = new \Phalcon\Mvc\Micro\Collection();
$booksCollection->setHandler('\App\Controllers\BooksController', true);
$booksCollection->setPrefix('/book');
$booksCollection->post('/add', 'addAction');
$booksCollection->get('/all', 'getBooksListAction');
$booksCollection->get('/search/{search:[^\&\/\?\#]*}', 'searchBooksListAction');
$booksCollection->get('/{bookId:[1-9][0-9]*}', 'getBookAction');
$booksCollection->put('/{bookId:[1-9][0-9]*}', 'updateBookAction');
$booksCollection->delete('/{bookId:[1-9][0-9]*}', 'deleteBookAction');
$app->mount($booksCollection);

// not found URLs
$app->notFound(
    function () use ($app) {
        $exception =
            new \App\Controllers\HttpExceptions\Http404Exception(
                _('URI not found or error in request.'),
                \App\Controllers\AbstractController::ERROR_NOT_FOUND,
                new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
            );
        throw $exception;
    }
);