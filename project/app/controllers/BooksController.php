<?php

namespace App\Controllers;

use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\BooksService;

/**
 * Operations with Books: CRUD
 */
class BooksController extends AbstractController
{
    /**
     * Adding book
     */
    public function addAction()
    {
        /** Init Block **/
        $errors = [];
        $data = [];
        $client = false;
        /** End Init Block **/

        /** Validation Block */
        $data['first_name'] = $this->request->getPost('first_name');
        if (!is_string($data['first_name']) || strlen($data['first_name']) > 32) {
            $errors['first_name'] = 'First Name must consists of not more than 32 symbols';
        }

        $data['last_name'] = $this->request->getPost('last_name');
        if (!is_null($data['last_name']) && (!is_string($data['last_name']) || strlen($data['last_name']) > 32)) {
            $errors['last_name'] = 'Last Name must consists of not more than 32 symbols';
        }

        $data['phone_number'] = $this->request->getPost('phone_number');
        if (!is_string($data['phone_number']) || !preg_match('/^\d{1,2} \d{3} \d{8,9}$/',$data['phone_number'])) {
            $errors['phone_number'] = 'Phone Number must be in 12 223 444224455 format'.$data['phone_number'];
        }

        $data['country_code'] = strtoupper($this->request->getPost('country_code'));
        if (!empty($data['country_code'])) {

            if (is_string($data['country_code']) && strlen($data['country_code']) == 2) {
                //validate country_code via external api
                $client = new \GuzzleHttp\Client(
                    [
                        'base_uri' => 'https://api.hostaway.com/'
                    ]
                );

                //get data from external api
                $response = $client->request('GET','countries');
                $content = \GuzzleHttp\json_decode($response->getBody(), true);

                if (!isset($content['status']) || $content['status'] !== 'success') {
                    $errors['country_code'] = 'Unable to get countries data from external api';
                } else if (!isset($content['result'][$data['country_code']])) {
                    $errors['country_code'] = 'Country Code in not valid';
                }
            } else {
                $errors['country_code'] = 'Country Code must consists of 2 symbols';
            }
        }


        $data['timezone_name'] = $this->request->getPost('timezone_name');
        if (!empty($data['timezone_name'])) {
            if (is_string($data['timezone_name']) && preg_match('/^\w+\/\w+$/',$data['timezone_name'])) {

                //validate timezone_name via external api
                if (!$client instanceof \GuzzleHttp\Client) {
                    $client = new \GuzzleHttp\Client(
                        [
                            'base_uri' => 'https://api.hostaway.com/'
                        ]
                    );
                }

                //get data from external api
                $response = $client->request('GET','timezones');
                $content = \GuzzleHttp\json_decode($response->getBody(), true);

                if (!isset($content['status']) || $content['status'] !== 'success') {
                    $errors['timezone_name'] = 'Unable to get timezones data from external api';
                } else if (!isset($content['result'][$data['timezone_name']])) {
                    $errors['timezone_name'] = 'Timezone Name in not valid';
                }
            } else {
                $errors['timezone_name'] = 'Timezone Name must be in Pacific/Midway format';
            }
        }

        if ($errors) {
            $exception = new Http400Exception( _('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        /** End Validation Block */

        /** Passing to business logic and preparing the response **/
        try {
            $this->booksService->createBook($data);
        } catch (\RuntimeException $e) {
            switch ($e->getCode()) {
                case BooksService::ERROR_UNABLE_CREATE_BOOK:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
        /** End Passing to business logic and preparing the response **/
    }

    /**
     * Returns book item
     *
     * @param $bookId
     *
     * @return array
     */
    public function getBookAction($bookId)
    {
        /** Init Block **/
        $errors = [];
        /** End Init Block **/

        /** Validation Block */
        if (!ctype_digit($bookId) || ($bookId < 0)) {
            $errors['id'] = 'Id must be a positive integer';
        }

        if ($errors) {
            $exception = new Http400Exception( _('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        /** End Validation Block */

        /** Passing to business logic and preparing the response **/
        try {
            $book = $this->booksService->getBook($bookId);
        } catch (\RuntimeException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }
        /** End Passing to business logic and preparing the response **/

        return $book;
    }


    /**
     * Returns book list
     *
     * @return array
     */
    public function getBooksListAction()
    {
        try {
            $bookList = $this->booksService->getBookList();
        } catch (\RuntimeException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $bookList;
    }

    /**
     * Returns book list
     *
     * @param $search
     *
     * @return array
     */
    public function searchBooksListAction($search = null)
    {
        try {
            $bookList = $this->booksService->searchBookList($search);
        } catch (\RuntimeException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $bookList;
    }



    /**
     * Updating existing book
     *
     * @param string $bookId
     */
    public function updateBookAction($bookId)
    {
        /** Init Block **/
        $errors = [];
        $data = [];
        $client = false;
        /** End Init Block **/

        /** Validation Block */
        $data['first_name'] = $this->request->getPut('first_name');
        if ((!is_null($data['first_name'])) && (!is_string($data['first_name']) || strlen($data['first_name']) > 32)) {
            $errors['first_name'] = 'First Name must consists of not more than 32 symbols';
        }

        $data['last_name'] = $this->request->getPut('last_name');
        if ((!is_null($data['last_name'])) && (!is_string($data['last_name']) || strlen($data['last_name']) > 32)) {
            $errors['last_name'] = 'Last Name must consists of not more than 32 symbols';
        }

        $data['phone_number'] = $this->request->getPut('phone_number');
        if ((!is_null($data['phone_number'])) && (!is_string($data['phone_number']) || !preg_match('/^\d{1,2} \d{3} \d{8,9}$/',$data['phone_number']))) {
            $errors['phone_number'] = 'Phone Number must be in +12 223 444224455 format';
        }

        $data['country_code'] = strtoupper($this->request->getPut('country_code'));
        if (!empty($data['country_code'])) {

            if (is_string($data['country_code']) && strlen($data['country_code']) == 2) {

                //validate country_code via external api
                $client = new \GuzzleHttp\Client(
                    [
                        'base_uri' => 'https://api.hostaway.com/'
                    ]
                );

                //get data from external api
                $response = $client->request('GET','countries');
                $content = \GuzzleHttp\json_decode($response->getBody(), true);

                if (!isset($content['status']) || $content['status'] !== 'success') {
                    $errors['country_code'] = 'Unable to get countries data from external api';
                } else if (!isset($content['result'][$data['country_code']])) {
                    $errors['country_code'] = 'Country Code in not valid';
                }
            } else {
                $errors['country_code'] = 'Country Code must consists of 2 symbols';
            }
        }


        $data['timezone_name'] = $this->request->getPut('timezone_name');
        if (!empty($data['timezone_name'])) {
            if (is_string($data['timezone_name']) && preg_match('/^\w+\/\w+$/',$data['timezone_name'])) {

                //validate timezone_name via external api
                if (!$client instanceof \GuzzleHttp\Client) {
                    $client = new \GuzzleHttp\Client(
                        [
                            'base_uri' => 'https://api.hostaway.com/'
                        ]
                    );
                }

                //get data from external api
                $response = $client->request('GET','timezones');
                $content = \GuzzleHttp\json_decode($response->getBody(), true);

                if (!isset($content['status']) || $content['status'] !== 'success') {
                    $errors['timezone_name'] = 'Unable to get timezones data from external api';
                } else if (!isset($content['result'][$data['timezone_name']])) {
                    $errors['timezone_name'] = 'Timezone Name in not valid';
                }
            } else {
                $errors['timezone_name'] = 'Timezone Name must be in Pacific/Midway format';
            }
        }

        if (!ctype_digit($bookId) || ($bookId < 0)) {
            $errors['id'] = 'Id must be a positive integer';
        }

        $data['id'] = (int)$bookId;


        if ($errors) {
            $exception = new Http400Exception( _('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        /** End Validation Block */

        /** Passing to business logic and preparing the response **/
        try {
            $this->booksService->updateBook($data);
        } catch (\RuntimeException $e) {
            switch ($e->getCode()) {
                case BooksService::ERROR_UNABLE_UPDATE_BOOK:
                case BooksService::ERROR_BOOK_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
        /** End Passing to business logic and preparing the response **/
    }

    /**
     * Delete an existing book
     *
     * @param string $bookId
     */
    public function deleteBookAction($bookId)
    {
        $errors = [];

        if (!ctype_digit($bookId) || ($bookId < 0)) {
            $errors['bookId'] = 'Id must be a positive integer';
        }

        if ($errors) {
            $exception = new Http400Exception( _('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try {
            $this->booksService->deleteBook($bookId);
        } catch (\RuntimeException $e) {
            switch ($e->getCode()) {
                case BooksService::ERROR_UNABLE_DELETE_BOOK:
                case BooksService::ERROR_BOOK_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(),$e->getCode(),$e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }
}