echo "============    BEGIN SETUP   ============="
echo -e "----------------------------------------"
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update
sudo apt-get install -y build-essential python-software-properties software-properties-common
sudo apt-get install -y re2c libpcre3-dev gcc make

#
# Install Git and Tools
#
echo -e "----------------------------------------"
echo "Git"
apt-get install -y git  > /dev/null

#
# Install Nginx
#
echo -e "----------------------------------------"
echo "Nginx"
apt-get install -y nginx  > /dev/null

echo -e "----------------------------------------"
sudo rm -rf /etc/nginx/sites-available/default
sudo rm -rf /etc/nginx/sites-enabled/default
echo "Setup Nginx"
cp /vagrant/src/nginx/project /etc/nginx/sites-available/project > /dev/null
ln -s /etc/nginx/sites-available/project /etc/nginx/sites-enabled/
sudo service nginx restart > /dev/null

#
# php
#
echo -e "----------------------------------------"
echo "PHP 7"
sudo apt-get install -y php7.0-fpm php7.0-cli php7.0-common php7.0-json php7.0-opcache php7.0-mysql php7.0-phpdbg php7.0-mbstring php7.0-gd php-imagick  php7.0-pgsql php7.0-pspell php7.0-recode php7.0-tidy php7.0-dev php7.0-intl php7.0-gd php7.0-curl php7.0-zip php7.0-xml php-memcached mcrypt memcached phpunit

#
# composer
#
echo -e "----------------------------------------"
echo "Composer"
curl -sS https://getcomposer.org/installer | php > /dev/null
mv composer.phar /usr/local/bin/composer

#
# MySQL
#
echo -e "----------------------------------------"
echo "MySQL"
apt-get install -y debconf-utils -y > /dev/null
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt install -y mysql-server mysql-client
sed -i 's/bind-address/bind-address = 0.0.0.0#/' /etc/mysql/mysql.conf.d/mysqld.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;"
service mysql restart

#
# Phalcon PHP Framework 3
#
echo -e "----------------------------------------"
echo "Setup Phalcon Framework"
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash
sudo apt-get update
sudo apt-get install php7.0-phalcon

cd /var/www/html/project/
sudo composer update

#
# Reload servers
#
echo -e "----------------------------------------"
echo "Restart Nginx & PHP-FPM"
sudo service nginx restart
sudo service php7.0-fpm restart

#
# COMPLETE
#
echo -e "----------------------------------------"
echo "VIRTUAL MACHINE READY"
echo "TYPE 'vagrant ssh"
echo -e "----------------------------------------"