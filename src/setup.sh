echo "============    BEGIN SETUP   ============="
echo -e "----------------------------------------"
sudo apt-get install -y language-pack-UTF-8
sudo apt-get install -y build-essential python-software-properties software-properties-common
sudo apt-get update
sudo apt-get install -y re2c libpcre3-dev gcc make


#
# Install Git and Tools
#
echo -e "----------------------------------------"
echo "Git"
apt-get install -y git  > /dev/null

echo -e "----------------------------------------"
echo "tools (mc, htop, unzip etc...)"
apt-get install -y mc htop unzip grc gcc make libpcre3 libpcre3-dev lsb-core autoconf > /dev/null

#
# Install Nginx
#
echo -e "----------------------------------------"
echo "Nginx"
apt-get install -y nginx  > /dev/null

echo -e "----------------------------------------"
sudo rm -rf /etc/nginx/sites-available/default
sudo rm -rf /etc/nginx/sites-enabled/default
echo "Setup Nginx"
cp /vagrant/src/nginx/project /etc/nginx/sites-available/project > /dev/null
ln -s /etc/nginx/sites-available/project /etc/nginx/sites-enabled/
sudo service nginx restart > /dev/null

#
# php
#
echo -e "----------------------------------------"
echo "PHP 7"
sudo apt-get install -y php7.0-fpm php7.0-cli php7.0-common php7.0-json php7.0-opcache php7.0-mysql php7.0-phpdbg php7.0-mbstring php7.0-gd php-imagick  php7.0-pgsql php7.0-pspell php7.0-recode php7.0-tidy php7.0-dev php7.0-intl php7.0-gd php7.0-curl php7.0-zip php7.0-xml php-memcached mcrypt memcached phpunit

#
# PHP Errors
#
echo -e "----------------------------------------"
echo "Setup PHP 7"
sudo sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php/7.0/fpm/php.ini
sudo sed -i 's/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL/' /etc/php/7.0/fpm/php.ini
sudo sed -i 's/display_startup_errors = Off/display_startup_errors = On/' /etc/php/7.0/fpm/php.ini
sudo sed -i 's/display_errors = Off/display_errors = On/' /etc/php/7.0/fpm/php.ini
#sudo sed -i 's/listen =/listen = 127.0.0.1:9000 ;/' /etc/php/7.0/fpm/pool.d/www.conf
service php7.0-fpm restart

#
# composer
#
echo -e "----------------------------------------"
echo "Composer"
curl -sS https://getcomposer.org/installer | php > /dev/null
mv composer.phar /usr/local/bin/composer

#
# MySQL
#
echo -e "----------------------------------------"
echo "MySQL"
export DEBIAN_FRONTEND=noninteractive
apt-get install -y debconf-utils -y > /dev/null
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt install -y mysql-server mysql-client
sed -i 's/bind-address/bind-address = 0.0.0.0#/' /etc/mysql/mysql.conf.d/mysqld.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;"
service mysql restart

#
# Phalcon PHP Framework 3
#
echo -e "----------------------------------------"
echo "Setup Phalcon Framework 3"
cd ~/
sudo apt-add-repository ppa:phalcon/stable
sudo apt-get update
sudo apt-get install -y php7.0-phalcon
echo 'extension=phalcon.so' > /etc/php/7.0/mods-available/phalcon.ini
ln -s /etc/php/7.0/mods-available/phalcon.ini /etc/php/7.0/fpm/conf.d/20-phalcon.ini

#
# Reload servers
#
echo -e "----------------------------------------"
echo "Restart Nginx & PHP-FPM"
sudo service nginx restart
sudo service php7.0-fpm restart

#
# Add user to group
#
sudo usermod -a -G www-data vagrant
#
# COMPLETE
#
echo -e "----------------------------------------"
echo "VIRTUAL MACHINE READY"
echo "TYPE 'vagrant ssh"
echo -e "----------------------------------------"